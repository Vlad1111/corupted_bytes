﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuzzySwitcher : MonoBehaviour
{
    public int speed = 50;
    public Image image;
    void FixedUpdate()
    {
        if (Random.Range(0, 100) / speed <= 0)
        {
            image.enabled = true;
        }
        else
        {
            image.enabled = false;
        }
    }
}