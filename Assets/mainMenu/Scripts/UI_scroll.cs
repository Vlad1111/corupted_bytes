﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_scroll : MonoBehaviour
{
    public Transform plate;
    private Vector3 originalLocation;
    private Scrollbar scroll;
    void Start()
    {
        scroll = GetComponent<Scrollbar>();
        originalLocation = plate.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        plate.localPosition = originalLocation + new Vector3(0, scroll.value, 0) * 100f;
    }
}
