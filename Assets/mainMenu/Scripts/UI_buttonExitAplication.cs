﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_buttonExitAplication : MonoBehaviour
{
    public Button yourButton;
    public GameObject parent;
    public bool startActive = true;

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        if (!startActive)
            parent.SetActive(false);
    }
    private void TaskOnClick()
    {
        //Debug.Log("PULA");
        Application.Quit();
        parent.SetActive(false);
    }
    /*public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Debug.Log("PULA");
            Application.Quit();
        }
    }*/
}
