﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_buttonMusicPlayOptions : MonoBehaviour
{
    public Button yourButton;
    public AudioSource fxSound;
    public Slider slider;
    public TextMeshProUGUI text;
    public bool isPlayein = false;

    void selectusicSorce()
    {
        GameObject[] allSorce = GameObject.FindGameObjectsWithTag("Music");
        if (allSorce.Length <= 0)
            return;
        int i = 0;
        while (fxSound == null && i < allSorce.Length)
            fxSound = allSorce[i++].GetComponent<AudioSource>();
        if (fxSound == null)
            return;
        slider.value = fxSound.volume;
    }

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        if (fxSound == null)
            selectusicSorce();
    }
    private void TaskOnClick()
    {
        if (fxSound == null)
            selectusicSorce();
        if (fxSound == null)
            return;
        if (isPlayein == true)
        {
            fxSound.Stop();
            text.text = ">";
        }
        else
        {
            fxSound.Play();
            text.text = "□";
        }
        isPlayein = !isPlayein;
    }
    private void FixedUpdate()
    {
        if (fxSound == null)
            selectusicSorce();
        if (fxSound == null)
            return;
        fxSound.volume = slider.value;
        if(fxSound.isPlaying)
        {
            isPlayein = true;
            text.text = "□";
        }
    }
}
