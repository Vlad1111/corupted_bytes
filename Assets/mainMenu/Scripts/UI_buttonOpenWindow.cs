﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonOpenWindow : MonoBehaviour {

    public Button yourButton;
    public GameObject window;

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        window.SetActive(!window.activeSelf);
    }
}
