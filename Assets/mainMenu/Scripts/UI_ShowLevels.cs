﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_ShowLevels : MonoBehaviour
{
    public Transform button;
    private SaveAndLoad SaL = null;
    private void Start()
    {
        if (SaL == null)
        {
            SaL = gameObject.AddComponent(typeof(SaveAndLoad)) as SaveAndLoad;
        }

        if (SaL.savedFile != null)
            for (int i = 0; i < SaL.savedFile.Length; i++)
            {
                RectTransform but = Instantiate(button, button.position + new Vector3(0, i * 10, 0),
                    button.rotation, transform).GetComponent<RectTransform>();
                but.anchoredPosition = new Vector2(but.anchoredPosition.x, but.anchoredPosition.y * i);

                UI_buttonChangeScheen sche = but.GetComponent<UI_buttonChangeScheen>();
                if (sche == null)
                    sche = but.gameObject.AddComponent(typeof(UI_buttonChangeScheen)) as UI_buttonChangeScheen;
                sche.yourButton = but.GetComponent<Button>();
                sche.map = SaL.savedFile[i];

                TextMeshProUGUI text = but.GetComponentInChildren<TextMeshProUGUI>();
                if (text != null)
                    text.text = SaL.savedFile[i];
            }
    }
}