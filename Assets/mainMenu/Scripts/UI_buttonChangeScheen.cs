﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_buttonChangeScheen : MonoBehaviour
{
    public int changeSceen = 1;
    public Button yourButton;
    public string map="";

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        if(map!="")
            PlayerPrefs.SetString("map", map);
        SceneManager.LoadScene(changeSceen);
    }
}
