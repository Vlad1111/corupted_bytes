﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonBehaviour : MonoBehaviour {

	public Material[] texture;
	private int actualTexture = 0;
	private MeshRenderer mesh;
	private bool isHovered = false;

	void OnMouseOver()
	{
		isHovered = true;
	}

	void Start()
	{
		mesh = gameObject.GetComponent<MeshRenderer> ();
	}
	// Update is called once per frame
	void FixedUpdate () {
		if (isHovered) {
			if(Random.Range (0, 100) < 10)
			actualTexture = Random.Range (1, 4);
		} else
			actualTexture = 0;
		mesh.material = texture [actualTexture%4];
		isHovered = false;
	}
}
