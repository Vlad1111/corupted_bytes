﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pipeServer_Unity : MonoBehaviour {
	private gameBehaviour server = null;
    int version;

	void Start()
	{
        if (server == null)
        {
            server = new gameBehaviour();
        }
        int auxBcFkIt = 0;
        version = server.getNrChanges(ref auxBcFkIt);
        //Debug.Log(server);
    }

    void FixedUpdate()
    {
        server.update();
    }

    public void getMap(int[,] getMap, ref int getHeight, ref int getWhide)
    {
        ///Get the map from server
        int[,] map;
        int HEIGHT, WHIDE;
        HEIGHT = WHIDE = 0;
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.getHEIGHT_WHIDE(ref HEIGHT, ref WHIDE);
        map = new int[HEIGHT, WHIDE];
        server.getMap(map, ref HEIGHT, ref WHIDE);


        ///Load the map further
        for (int i = 0; i < HEIGHT; i++)
            for (int j = 0; j < WHIDE; j++)
                getMap[i, j] = map[i, j];
        getHeight = HEIGHT;
        getWhide = WHIDE;
    }

	public void getHEIGHT_WHIDE(ref int getHeight, ref int getWhide)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        ///Get the map size from server
        int HEIGHT, WHIDE;
        HEIGHT = WHIDE = 0;
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.getHEIGHT_WHIDE(ref HEIGHT, ref WHIDE);

        ///Load the map size further
        getHeight = HEIGHT;
		getWhide = WHIDE;
	}

    internal void loadServerData(World world)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.loadServerData(world);
    }

    public int getNrChanges()
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        int auxBcFkIt = 0;
        return server.getNrChanges(ref auxBcFkIt);
    }

    public void getNrNPCS(ref int nrNPC)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        nrNPC = server.getNrNPC();
    }

    public void getNPCS(NPC[] NPCS, ref int nrNPC)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        nrNPC = server.getNPCS(NPCS);
    }

    public void getNPCPosition(ref float x, ref float y,int index)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.getNPCPosition(ref x, ref y, index);
    }

    public void updateNPCS(Transform[] NPCS, int nrNPC)
    {
        for(int i=0;i<nrNPC;i++)
        {
            float x, y;
            x = y = 0;
            server.getNPCPosition(ref x, ref y, i);
            NPCS[i].localPosition = new Vector3(y, 0, x);
        }
    }

    public void updatePositionNPC(ref float x, ref float y, int index)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.getNPCPosition(ref x, ref y, index);
    }

    public void playerClickedOn(int x, int y, int whatActon, int NPC_clicked)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.playerChouseObject(x, y, whatActon, NPC_clicked);
    }

    public int updateWord(ref Transform[] nPCS, int nrNPC, ref Transform word, Transform[] objMesh)
    {
        int auxBcFkIt = 0;
        int actualVersion = server.getNrChanges(ref auxBcFkIt);
        for(int i=version;i<actualVersion;i++)
        {
            ServerChanges change = server.getChange(i);
            if (change == null)
                continue;
            int x, y;
            x = y = -1;
            change.getPosition(ref x, ref y);

            if (change.getNPC() >= 0)
            {
                if (change.getType() == 0)
                {
                    nPCS[change.getNPC()].GetComponent<NPC_movement>().moveTo(x, y);
                }
            }
            else
            {
                Transform child = word.Find(x + "_" + y);
                if (child == null)
                    continue;
                if (change.getType() == 1)
                {
                    Destroy(child.gameObject);
                    Instantiate(objMesh[-change.getNPC()-1], new Vector3(x, 0, y), objMesh[0].transform.localRotation, word).name = x + "_" + y;
                }
                else if(change.getType() == -1) // game won
                {
                    return - 1;
                }
               // else if (change.getType() == 6)
               // {
               //     Destroy(child.gameObject);
               //     Instantiate(objMesh[0], new Vector3(x, 0, y), objMesh[0].transform.localRotation, word).name = x + "_" + y;
              //  }
            }
        }
        /*for (int i = 0; i < nrNPC; i++)
        {
            float x, y;
            x = y = 0;
            server.getNPCPosition(ref x, ref y, i);
            nPCS[i].GetComponent<NPC_movement>().moveTo(x, y);
        }*/
        version = actualVersion;
        return 0;
    }

    public void getNPCInfo(int indexNPC, int[] data, ref int power, ref int key)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.getInfoNPC(indexNPC, data, ref power, ref key);
    }

    public bool isNeddedUpdate(ref int playerEnergy)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        return version == server.getNrChanges(ref playerEnergy);
    }

    public void getDataObject(int x, int y, int[] data, ref int distroyCost, ref int key)
    {
        if (server == null)
        {
            server = new gameBehaviour();
        }
        server.getDataInventoryObject(x, y, data, ref distroyCost, ref key);
    }
}
