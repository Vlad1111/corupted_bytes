﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class createWord : MonoBehaviour
{
    public objectMeniuObtions obtionMenu; // meniul de obtiuni
    public DataInventory dataInventory; // meniul de inventar al obiectului
    public Transform[] objs; // mesh-urile obiectelor din joc
    public Transform[] NPCS_Mesh; // mesh-urile NPC-urilor din joc
    public GameObject winScreen; // win screen

    public TextMeshProUGUI enegyText; // textul de la UI-ul pt energia jucatorului
    //public Image enegyImage; // "progress" barul de la UI-ul pt enegria jucatorului


    private Transform[] NPCS; ///TOATE NPC-urile -> NPCS[0] e playerul
    private int nrNPC = 0;
	private pipeServer_Unity cmds;
    private bool winGame = false;
    private SaveAndLoad SaL; // the class to load and save the map

    void Start()
    {
        cmds = GetComponent<pipeServer_Unity>();
        if (cmds == null)
        {
            cmds = gameObject.AddComponent(typeof(pipeServer_Unity)) as pipeServer_Unity;
        }
        if (PlayerPrefs.HasKey("map"))
        {
            string loadThis = PlayerPrefs.GetString("map");
            if (SaL == null)
            {
                SaL = gameObject.AddComponent(typeof(SaveAndLoad)) as SaveAndLoad;
            }
            if (SaL.saveTo == "")
                SaL.setSaveTo();
            SaL.worldToSave.size = new WorldSizes();
            SaL.Load(loadThis);
            if (SaL.worldToSave.size.WHEID!=0 && SaL.worldToSave.size.HEIGHT != 0)
                cmds.loadServerData(SaL.worldToSave);
        }
        ///Load map from the server
        int[,] map;
        int HEIGHT, WHIDE;
        HEIGHT = WHIDE = 0;
        cmds.getHEIGHT_WHIDE(ref HEIGHT, ref WHIDE);
        map = new int[HEIGHT, WHIDE];
        cmds.getMap(map, ref HEIGHT, ref WHIDE);
        for (int i = 0; i < HEIGHT; i++)
        {
            for (int j = 0; j < WHIDE; j++)
            {
                Instantiate(objs[map[i, j]], new Vector3(i, 0, j), objs[map[i, j]].transform.localRotation, transform).name = i + "_" + j;
            }
        }
        ///create the NPC + player
        cmds.getNrNPCS(ref nrNPC);
        NPCS = new Transform[nrNPC];
        NPC[] servNPC = new NPC[nrNPC];
        cmds.getNPCS(servNPC, ref nrNPC);
        for(int i=0;i<nrNPC;i++)
        {
            float x, y;
            x = y = 0;
            servNPC[i].getPosition(ref x, ref y);
            NPCS[i] = Instantiate(NPCS_Mesh[servNPC[i].getType()], new Vector3(x, 0, y), 
                NPCS_Mesh[servNPC[i].getType()].transform.localRotation, transform);
        }

        ///getThePlayerScript
        /*foreach (Transform child in transform)
        {
            player = child.GetComponent<playerAction>();
            if (player != null)
                break;
        }
        if (player == null)
            return;*/
    }

    public void moveNPCTo(float x, float y, int index)
    {
        NPC_movement NPC_m = NPCS[index].GetComponent<NPC_movement>();
        if (NPC_m != null)
            NPC_m.moveTo(x, y);
    }

    void FixedUpdate()
    {
        /*
        for(int i=0;i<nrNPC;i++)
        {
            float x, y;
            x = y = 0;
            cmds.getNPCPosition(ref x, ref y, i);
            NPCS[i].GetComponent<NPC_movement>().moveTo(x, y);
        }*/
        int energy = 100;
        if (!cmds.isNeddedUpdate(ref energy) && !winGame)
        {
            Transform trans = GetComponent<Transform>();
            int ret = cmds.updateWord(ref NPCS, nrNPC, ref trans, objs);
            if(ret == -1)
            {
                winGame = true;
                winScreen.SetActive(true);
            }
        }
        enegyText.text = "Energy:  " + energy;
        //enegyImage.transform.localScale = new Vector3(energy / 100f, enegyImage.transform.localScale.y, enegyImage.transform.localScale.z);
    }

    public Camera getThePlayerCamera() ///for the direction change of the camera in the ui
    {
        if (NPCS == null)
            return null;
        return NPCS[0].transform.GetComponent<playerBehaviour>().getPlayerCamera(NPCS[0].transform);
    }

    public void playerClickedOn(int x, int y, int whatAction, int NPC_clicked)
    {
        cmds.playerClickedOn(x, y, whatAction, NPC_clicked);
    }

    public void getDataObject(int x, int y)
    {
        int[] a;
        a = new int[nrNPC+1];
        int distroyCost = 0;
        int key = 0;
        cmds.getDataObject(x, y, a, ref distroyCost, ref key);
        dataInventory.setData(a, "Distroy cost:   " + distroyCost+ "\nAdmin Level:  " + key + "\nData Containging:  \n");
    }

    public void getPlayersInfo(int[] data, ref int power, ref int key)
    {
        cmds.getNPCInfo(0, data, ref power, ref key);
    }
}
