﻿public class ServerChanges
{
    private int type;
    private int x, y;
    private int NPC_index;

    public ServerChanges(int t,int X, int Y, int NPC_i)
    {
        type = t;
        x = X;
        y = Y;
        NPC_index = NPC_i;
    }

    public int getType()
    {
        return type;
    }
    public void getPosition(ref int x, ref int y)
    {
        x = this.x;
        y = this.y;
    }
    public int getNPC()
    {
        return NPC_index;
    }
}
