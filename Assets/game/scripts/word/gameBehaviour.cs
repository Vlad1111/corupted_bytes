﻿using System;
using System.Collections.Generic;
public class gameBehaviour
{
    private int nrDate = 10;
    private GameObjectBehaviour[,] map;
    private int HEIGHT, WHIDE;
    private int nrNPC;
    private NPC[] NPCS;
    private int alertLevel = 0;
    private int antivirusesPower = 100;
    List<ServerChanges> changes;
    Random ran = new Random();

    private bool map_isBlocPasseble(int ID)
    /*
     * If a block id is for a block that can be stand on
     */
    {
        if (ID == 0 || ID == 5)
            return true;
        //if (ID == 1 || ID == 2 || ID == 3)
        //    return false;
        return false;
    }

    private GameObjectBehaviour getNewObj(int x)
    {
        int[] data = new int[nrDate];
        if (x == 0) // podea
            return new GameObjectBehaviour(x, 0, data, 0);
        else if (x == 1) // perete
            return new GameObjectBehaviour(x, 100000, data, 50);
        else if (x == 2) // generator
            return new GameObjectBehaviour(x, 100000, data, -20);
        else if (x == 3) // kernel
            return new GameObjectBehaviour(x, 100000, data, 20);
        else if (x == 4) // gate
            return new GameObjectBehaviour(x, 4, data, 35);
        else if (x == 5) // quarantine not used
            return new GameObjectBehaviour(x, 0, data, 10);
        else if (x == 6) // quarantine used
            return new GameObjectBehaviour(x, 100000, data, 20);
        else if (x == 7) // spowner
        {
            data[0] = 5;
            GameObjectBehaviour obj = new GameObjectBehaviour(x, 100000, data, 30);
            return obj;
        }
        else if (x == 8) // core
            return new GameObjectBehaviour(x, 0, data, 0);
        return new GameObjectBehaviour();
    }

    private void makeMazeFFS(int x, int y, int size)
    {
        map[x, y] = new GameObjectBehaviour(0, 0);
        if (size > 100)
            return;
        for (int d = 0; d < 4; d++)
            if (ran.Next(0, 4) != 2)
            {
                int xx, yy;
                xx = x + 2 * generalTerms.d1[d];
                yy = y + 2 * generalTerms.d2[d];
                if (xx > 0 && xx < HEIGHT - 1 && yy > 0 && yy < WHIDE - 1 && map[xx, yy].getType() != 0)
                {
                    map[x + generalTerms.d1[d], y + generalTerms.d2[d]] = new GameObjectBehaviour(0, 0);
                    makeMazeFFS(xx, yy, size + 1);
                }
                else if (ran.Next(0, 10) == 2)
                {
                    xx = x + generalTerms.d1[d];
                    yy = y + generalTerms.d2[d];
                    if (xx > 0 && xx < HEIGHT - 1 && yy > 0 && yy < WHIDE - 1 && map[xx, yy].getType() != 0)
                    {
                        map[xx, yy] = new GameObjectBehaviour(0, 0);
                    }
                }
                else if (ran.Next(0, 10) == 2)
                {
                    xx = x + generalTerms.d1[d];
                    yy = y + generalTerms.d2[d];
                    if (xx > 0 && xx < HEIGHT - 1 && yy > 0 && yy < WHIDE - 1 && map[xx, yy].getType() != 0)
                    {
                        map[xx, yy] = new GameObjectBehaviour(ran.Next(2, 9), 10000);
                    }
                }
            }
    }

    internal void loadServerData(World world)
    {
        HEIGHT = world.size.HEIGHT;
        WHIDE = world.size.WHEID;
        nrNPC = world.size.nrNPC+1;
        NPCS = new NPC[nrNPC+1];
        for (int i = 1; i < nrNPC; i++)
            NPCS[i] = world.NPCS[i-1];
        NPCS[0] = new NPC(0, 100, 0, 1, 1, 10, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
        map = new GameObjectBehaviour[HEIGHT, WHIDE];
        for (int i = 0; i < HEIGHT; i++)
            for (int j = 0; j < WHIDE; j++)
                map[i, j] = getNewObj(world.map[i, j]);
    }

    public gameBehaviour()
    {
        changes = new List<ServerChanges>();
        HEIGHT = generalTerms.map_HEIGHT;
        WHIDE = generalTerms.map_WHIDE;
        map = new GameObjectBehaviour[HEIGHT, WHIDE];
        for (int i = 0; i < HEIGHT; i++)
            for (int j = 0; j < WHIDE; j++)
            {
                map[i, j] = new GameObjectBehaviour(1, 0);
            }

         makeMazeFFS(1, 1, 0);

        /*for (int i = 0; i < 10; i++)
        {
            map[i, 7] = new GameObjectBehaviour(1, 10000, new int[] { }, 50);
            map[i, 15] = new GameObjectBehaviour(5, 0, new int[] { }, 50);
           // map[i, 20] = new GameObjectBehaviour(7, 10000, new int[] { }, 50);
        }
        map[0, 0] = new GameObjectBehaviour(7, 10000, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 50);
        map[5, 10] = new GameObjectBehaviour(2, 10000, new int[] { }, -20);
        map[5, 5] = new GameObjectBehaviour(3, 10000, new int[] { 10, 0 }, 5);
        map[5, 7] = new GameObjectBehaviour(4, 10, new int[] { 0, 0 }, 25);
        map[generalTerms.map_HEIGHT/2, generalTerms.map_WHIDE/2] = new GameObjectBehaviour(8, 100000, new int[] { 0, 0 }, 0);*/

        //makeMazeFFS(0, 0, 0);

        nrNPC = 4;
        NPCS = new NPC[nrNPC];
        for (int i = 0; i < nrNPC; i++)
        {
            NPCS[i] = new NPC(0, 100, 1, 1, 1, nrDate, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
        }
        NPCS[1] = new NPC(1, 0, 20, 10, 10, nrDate, new int[] {0, 1});
        NPCS[1].setDestinations(new int[] { 5, 5}, new int[] { 5, 10});
        NPCS[2] = new NPC(3, 0, 20, 10, 10, nrDate, new int[] { 0, 0 });
        NPCS[2].setDestinations(new int[] { 5, 0 }, new int[] { 5, 0 });
        NPCS[3] = new NPC(2, 0, 20, 20, 20, 2, new int[] { 0, 0 });
        NPCS[3].setDestinations(new int[] { 20, 20 }, new int[] { 20, 40 });
    }
    public void update()
    {
        float x, y;
        x = y = -1;
        for (int i = 0; i < nrNPC; i++)
        {
            NPCS[i].getPosition(ref x, ref y);
            if (map[(int)x, (int)y].getType() == 6 && NPCS[i].getType() != 2)
            {
                NPCS[i].restartPosition(true);
                continue;
            }
            else if (map[(int)x, (int)y].getType() == 5 && NPCS[i].getType() != 2)
            {
                map[(int)x, (int)y] = new GameObjectBehaviour(6, 10000, new int[] { }, 20);
                NPCS[i].restartPosition(true);
                changes.Add(new ServerChanges(1, (int)x, (int)y, -7));
                continue;
            }

            if (NPCS[i].getType() == 2 && alertLevel != 0)
            {
                if (NPCS[i].getSteps() == 1)
                {
                    /*int[] transpitorData = new int[2];
                    int distrycost = 0, security = 0;
                    int[] xxx, yyy;
                    xxx = NPCS[i].getDestinationsX();
                    yyy = NPCS[i].getDestinationsY();
                    map[xxx[0], yyy[0]].getData(transpitorData, ref distrycost, ref security);
                    if (xxx.Length >= 1 && map[xxx[0], yyy[0]].getType() == 7 && transpitorData[0] >= 1)
                    {
                        int[] moveToX, moveToY;
                        moveToX = NPCS[i].getmovementX();
                        moveToY = NPCS[i].getmovementY();
                        int[] destinationX, destinationY;
                        destinationX = NPCS[i].getDestinationsX();
                        destinationY = NPCS[i].getDestinationsY();
                        int len = destinationX.Length;
                        if (len > destinationY.Length)
                            len = destinationY.Length;

                        if ((moveToX[0] != destinationX[(len + NPCS[i].getDestinationIndex() - 1) % len] ||
                            moveToY[0] != destinationY[(len + NPCS[i].getDestinationIndex() - 1) % len]) &&
                            map[moveToX[0], moveToY[0]].getType() == 0)
                        {
                            alertLevel = 0;
                            map[moveToX[0], moveToY[0]] = getNewObj(5);
                            map[xxx[0], yyy[0]].addData(0, -1);
                            changes.Add(new ServerChanges(1, moveToX[0], moveToY[0], -6));
                            NPCS[i].restartPosition(true);
                        }
                    }*/
                    int[] xxx, yyy;
                    xxx = NPCS[i].getDestinationsX();
                    yyy = NPCS[i].getDestinationsY();
                    if(xxx.Length > 0 && yyy.Length > 0)
                        if(map[xxx[0],yyy[0]].getType() == 7)
                        {
                            int[] data = new int[2];
                            int distrycost = 0, security = 0;
                            map[xxx[0], yyy[0]].getData(data, ref distrycost, ref security);
                            if(data[0] > 0)
                            {
                                int[] moveToX, moveToY;
                                moveToX = NPCS[i].getmovementX();
                                moveToY = NPCS[i].getmovementY();
                                int[] destinationX, destinationY;
                                destinationX = NPCS[i].getDestinationsX();
                                destinationY = NPCS[i].getDestinationsY();
                                int len = destinationX.Length;
                                if (len > destinationY.Length)
                                    len = destinationY.Length;

                                int ok = 0;
                                string st = "";
                                for (int iii = 0; iii < len; iii++)
                                {
                                    if (moveToX[0] == destinationX[iii] && moveToY[0] == destinationY[iii])
                                        ok = 1;
                                    st += destinationX[iii] + "   " + destinationY[iii] + " ~~ ";
                                }

                                if (ok == 0 && map[moveToX[0], moveToY[0]].getType() == 0)
                                {
                                    /*throw new Exception(st + moveToX[0] + "  " + moveToY[0]);*/
                                    alertLevel = 0;
                                    map[moveToX[0], moveToY[0]] = getNewObj(5);
                                    map[xxx[0], yyy[0]].addData(0, -1);
                                    changes.Add(new ServerChanges(1, moveToX[0], moveToY[0], -6));
                                    NPCS[i].restartPosition(true);
                                }
                            }
                        }
                }
            }

            int ret = 0;
            //if (NPCS[i].getType() != 2)
            ret = NPCS[i].nextStep();
            
            if (ret == 0)
            {
                NPCS[i].getPosition(ref x, ref y);
                changes.Add(new ServerChanges(0, (int)x, (int)y, i));
            }
            else if(ret == 2 && i!=0)
            {
                int[] X = NPCS[i].getDestinationsX();
                int[] Y = NPCS[i].getDestinationsY();
                int index = NPCS[i].getDestinationIndex();

                if(index < X.Length && index < Y.Length)
                {
                    int xxxx, yyyy;
                    xxxx = (X.Length + index - 1) % X.Length;
                    yyyy = (Y.Length + index - 1) % Y.Length;
                    if (map[X[xxxx], Y[yyyy]].getType() == 3)
                    {
                        if (NPCS[i].getRemaingingPower() > 20 && NPCS[i].getType() == 1)
                        {
                            map[X[xxxx], Y[yyyy]].addData(i % nrDate, 1);
                            NPCS[i].changePower(-20);
                        }
                        else if(NPCS[i].getType() == 3)
                        {
                            int[] newData = map[X[xxxx], Y[yyyy]].extractData();
                            NPCS[i].addData(newData);
                        }
                    }
                    else if (map[X[xxxx], Y[yyyy]].getType() == 2)
                    {
                        NPCS[i].changePower(-map[X[xxxx], Y[yyyy]].getDistroyCost());
                    }
                    else if (map[X[xxxx], Y[yyyy]].getType() == 7)
                    {
                        int[] spwonerData = new int[2];
                        int distrycost = 0, security = 0;
                        map[X[xxxx], Y[yyyy]].getData(spwonerData, ref distrycost, ref security);
                        if(spwonerData[0] < 5)
                        {
                            int[] NPCdata = NPCS[i].getData();
                        
                            int suma = 0;
                            int[] newData = new int[NPCdata.Length];
                            int[] minusData = new int[NPCdata.Length];
                            for (int j = 0; j < NPCdata.Length; j++)
                            {
                                /*if (NPCdata[i] > 0)
                                {
                                    antivirusesPower += NPCdata[i];
                                }*/
                                if (NPCdata[j] > 0)
                                    suma += NPCdata[j];
                                newData[j] = 0;
                                minusData[j] = -1 * NPCdata[j];
                            }
                            newData[0] = suma;
                            map[X[xxxx], Y[yyyy]].addData(newData);
                            NPCS[i].addData(minusData);

                            map[X[xxxx], Y[yyyy]].getData(spwonerData, ref distrycost, ref security);
                            if(spwonerData[0]>5)
                            {
                                newData[0] = 5 - spwonerData[0];
                                map[X[xxxx], Y[yyyy]].addData(newData);
                            }
                        }
                    }

                    moveNPCto(X[index], Y[index], false, i);
                    NPCS[i].setDestinationIndex(index + 1);
                    //changes.Add(new ServerChanges(0, (int)X[index], (int)Y[index], i));
                }
                else
                    NPCS[i].setDestinationIndex(0);
            }
        }
    }

    public void getInfoNPC(int indexNPC, int[] data, ref int power, ref int key)
    {
        int[] newData = NPCS[indexNPC].getData();
        power = NPCS[indexNPC].getRemaingingPower();
        key = NPCS[indexNPC].getSecurityKey();
        for (int i = 0; i < newData.Length && i < data.Length; i++)
            data[i] = newData[i];
        for (int i = newData.Length; i < data.Length; i++)
            data[i] = 0;
    }

    public int getNrChanges(ref int playerEnergy)
    {
        playerEnergy = NPCS[0].getRemaingingPower();
        return changes.Count;
    }
    public ServerChanges getChange(int index)
    {
        if (index < 0 || index >= changes.Count)
            return null;
        return changes[index];
    }

    public void getHEIGHT_WHIDE(ref int getHeight, ref int getWhide)
    {
        getHeight = HEIGHT;
        getWhide = WHIDE;
    }

    public void getMap(int[,] getMap, ref int getHeight, ref int getWhide)
    {
        for (int i = 0; i < HEIGHT; i++)
            for (int j = 0; j < WHIDE; j++)
                getMap[i, j] = map[i, j].getType();
        getHeight = HEIGHT;
        getWhide = WHIDE;
    }

    public int getNrNPC()
    {
        return nrNPC;
    }

    public int getNPCS(NPC[] NPCs)
    {
        for (int i = 0; i < nrNPC; i++)
            NPCs[i] = NPCS[i];
        return nrNPC;
    }
    public void getNPCPosition(ref float x, ref float y, int index)
    {
        NPCS[index].getPosition(ref x, ref y);
    }

    public void playerChouseObject(int x, int y, int whatDoesHeWant, int NPC_clickedOn)
    /*
     * The player clicked on this object
     * wahtDoesHeWant ->     0 - to move to
     *                       1 - to distroy
     *                       2 - to take energy from and to take data from
     *                       3 - to increase the admin level
     *                       4 - to create personal data
     *  NPC_clickedOn -> the NPC that the player clicked on (0 if it was an game object)
     */
    {
        if (x < 0 || y < 0 || x >= HEIGHT || y >= WHIDE)
            return;
        if (NPC_clickedOn != 0)
        {

        }
        else
        {
            if (whatDoesHeWant == 0)
            {
                moveNPCto(x, y, false, 0);
                //if (movePlayerTo(x, y, true) == 0)
                //    changes.Add(new ServerChanges(0, x, y, 0));
            }
            else if (whatDoesHeWant == 1 && NPCS[0].getRemaingingPower()>=map[x, y].getDistroyCost())
            {
                if (moveNPCto(x, y, false, 0) == 2)
                {
                    //throw new System.Exception(x + " " + y);
                    NPCS[0].changePower(-map[x, y].getDistroyCost());
                    if (map[x, y].getType() == 8)
                    {
                        changes.Add(new ServerChanges(-1, x, y, -1));
                        return;
                    }
                    map[x, y] = new GameObjectBehaviour(0, 0);
                    changes.Add(new ServerChanges(1, x, y, -1));
                    alertLevel = 100;
                    for (int i = 0; i < nrNPC; i++)
                        if (NPCS[i].getType() == 2)
                        {
                            //int[] data = NPCS[i].getData();
                            //if (map[data[0], data[1]].getType() == 7)
                            {
                                //int sum = 0;

                                moveNPCto(x, y, false, i);
                            }
                        }
                }
            }
            else if (whatDoesHeWant == 2 && NPCS[0].getRemaingingPower()>=map[x, y].getDistroyCost())
            {
                if (moveNPCto(x, y, false, 0) == 2)
                {
                    //throw new System.Exception(x + " " + y);
                    //NPCS[0].changePower(-map[x, y].getDistroyCost());
                    //map[x, y] = new GameObjectBehaviour(0, 0);
                    //changes.Add(new ServerChanges(1, x, y, -1));
                    int[] data = map[x, y].extractData();
                    NPCS[0].addData(data);
                    NPCS[0].changePower(-map[x, y].getDistroyCost());
                    if (map[x, y].getDistroyCost() < 0 && NPCS[0].getRemaingingPower() > 100)
                    {
                        NPCS[0].changePower(-NPCS[0].getRemaingingPower() + 100);
                        //NPCS[0].changePower(-map[x,y].getDistroyCost());
                        //playerChouseObject(x, y, 1, NPC_clickedOn);
                    }
                }
            }
            else if (whatDoesHeWant == 3)
            {
                //throw new Exception("FUCK IT");
                int key = NPCS[0].getSecurityKey();
                if (NPCS[0].getData()[0] >= key * key)
                {
                    int[] newData = new int[NPCS[0].getData().Length];
                    newData[0] = -key * key;
                    NPCS[0].addData(newData);
                    NPCS[0].changeSecurityKey(key + 1);
                }
            }
            else if (whatDoesHeWant == 4)
            {
                if (NPCS[0].getRemaingingPower() < 10)
                    return;
                int ok = 0;
                int[] data = NPCS[0].getData();
                for(int i=1;i<data.Length;i++)
                    if(data[i] >= 10)
                    {
                        ok = i;
                        break;
                    }
                if(ok != 0)
                {
                    int[] minusData = new int[data.Length];
                    minusData[ok] = -10;
                    minusData[0] = 10;
                    NPCS[0].addData(minusData);
                    NPCS[0].changePower(-10);
                }
            }
        }
    }

    private int moveNPCto(int x, int y, bool goStreightTo, int indexNPC)
    /*
     * Errorrs : 1 - the parameters are invalid
     *           2 - is the same position
     *           3 - there is no path
     */
    {
        if (x < 0 || x >= HEIGHT || y < 0 || y >= WHIDE)
            return 1;
        int xx, yy;

        int[,] aux_map = new int[HEIGHT, WHIDE];
        float[] mvX, mvY;
        mvX = new float[HEIGHT * WHIDE + 10];
        mvY = new float[HEIGHT * WHIDE + 10];
        int inc, sfr;
        inc = sfr = 0;
        sfr = 1;
        NPCS[indexNPC].getPosition(ref mvX[0], ref mvY[0]);
        if (mvX[0] == x && mvY[0] == y)
            return 2;
        for (int i = 0; i < HEIGHT; i++)
            for (int j = 0; j < WHIDE; j++)
                aux_map[i, j] = -1;
        aux_map[(int)mvX[0], (int)mvY[0]] = 1;
        while (inc < sfr)
        {
            xx = (int)mvX[inc];
            yy = (int)mvY[inc];
            inc++;
            for (int d = 0; d < 4; d++)
            {
                int xxx, yyy;
                xxx = xx + generalTerms.d1[d];
                yyy = yy + generalTerms.d2[d];

                if (xxx >= 0 && xxx < HEIGHT && yyy >= 0 && yyy < WHIDE)
                    if (aux_map[xxx, yyy] == -1 && 
                        (map_isBlocPasseble(map[xxx, yyy].getType()) || map[xxx, yyy].getWallValue() < NPCS[indexNPC].getSecurityKey())
                        && (NPCS[indexNPC].getType() != 2 || map[xxx, yyy].getType()!=5))
                    {
                        aux_map[xxx, yyy] = aux_map[xx, yy] + 1;
                        mvX[sfr] = xxx;
                        mvY[sfr] = yyy;
                        sfr++;
                    }
                    else if (xxx == x && yyy == y)
                    {
                        inc = sfr + 1;
                    }
            }
        }

        if (aux_map[x, y] == -1 && goStreightTo)
            return 3;
        else if (aux_map[x, y] == -1)
        {
            xx = yy = -1;
            for (int d = 0; d < 4; d++)
            {
                int xxx, yyy;
                xxx = x + generalTerms.d1[d];
                yyy = y + generalTerms.d2[d];
                if (xxx >= 0 && xxx < HEIGHT && yyy >= 0 && yyy < WHIDE)
                    if (aux_map[xxx, yyy] != -1)
                    {
                        if (xx == -1 && xx == -1)
                        {
                            xx = xxx;
                            yy = yyy;
                        }
                        else if (aux_map[xx, yy] > aux_map[xxx, yyy])
                        {
                            xx = xxx;
                            yy = yyy;
                        }
                    }
            }
            if (mvX[0] == xx && mvY[0] == yy)
                return 2;
            x = xx;
            y = yy;
            //throw new System.Exception(x + " " + y + "  |  " + xx + " " + yy);
            if (x == -1 && y == -1)
                return 3;
        }

        mvX = new float[HEIGHT * WHIDE + 10];
        mvY = new float[HEIGHT * WHIDE + 10];
        int nrSteps = 0;

        float plPozX, plPozY;
        plPozY = plPozX = 0;
        NPCS[indexNPC].getPosition(ref plPozX, ref plPozY);
        mvX[nrSteps] = x;
        mvY[nrSteps] = y;

        while ((int)mvX[nrSteps] != (int)plPozX || (int)mvY[nrSteps] != (int)plPozY)
        {
            for (int d = 0; d < 4; d++)
            {
                int xxx, yyy;
                xxx = (int)mvX[nrSteps] + generalTerms.d1[d];
                yyy = (int)mvY[nrSteps] + generalTerms.d2[d];

                if (xxx >= 0 && xxx < HEIGHT && yyy >= 0 && yyy < WHIDE)
                    if (aux_map[xxx, yyy] + 1 == aux_map[(int)mvX[nrSteps], (int)mvY[nrSteps]])
                    {
                        nrSteps++;
                        mvX[nrSteps] = xxx;
                        mvY[nrSteps] = yyy;
                        break;
                    }
            }
        }

        NPCS[indexNPC].restartPosition(true);
        for (int i = nrSteps - 1; i >= 0; i--)
            NPCS[indexNPC].pushMovement((int)mvX[i], (int)mvY[i]);
        return 0;
    }

    public void getDataInventoryObject(int x, int y, int[] data, ref int distroyCost, ref int key)
    {
        map[x, y].getData(data, ref distroyCost, ref key);
    }
}
