﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonIncreaseAdminLevel : MonoBehaviour
{
    public Button yourButton;
    public createWord word;
    public int wahtToDo;

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        word.playerClickedOn(0, 0, wahtToDo, 0);
    }
}
