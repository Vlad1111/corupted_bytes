﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_updatePersonalData : MonoBehaviour
{
    public TextMeshProUGUI text;
    public createWord world;

    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        int[] personalData = new int[10];
        int securityKey = 1, power = 100;
        world.getPlayersInfo(personalData, ref power, ref securityKey);
        string str = "";
        str += "Power:   " + power + '\n';
        str += "Key:   " + securityKey +"\n(next level for " + (securityKey*securityKey) + " and 10 power)\n";
        for (int i = 0; i < personalData.Length; i++)
            if (personalData[i] != 0)
            {
                str += i + ".   " + personalData[i] + '\n';
            }
        text.text = str;
    }
}
