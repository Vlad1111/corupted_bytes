﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonExitObtionMenu : MonoBehaviour
{
    public Button yourButton;
    public GameObject parent;
    public bool startActive = true;

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        if(!startActive)
         parent.SetActive(false);
    }
    private void TaskOnClick()
    {
        parent.SetActive(false);
    }
}
