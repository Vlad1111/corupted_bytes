﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonGetDataInventry : MonoBehaviour
{
    public Button yourButton;
    public GameObject parent;

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        createWord word = GetComponentInParent<objectMeniuObtions>().takeCamera;
        Transform target = GetComponentInParent<objectMeniuObtions>().target;
        word.getDataObject((int)target.localPosition.x, (int)target.localPosition.z);
        parent.SetActive(false);
    }
}
