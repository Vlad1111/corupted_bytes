﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonExtractData : MonoBehaviour
{
    public Button yourButton;
    public GameObject parent;

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        createWord word = GetComponentInParent<objectMeniuObtions>().takeCamera;
        Transform target = GetComponentInParent<objectMeniuObtions>().target;
        word.playerClickedOn((int)target.localPosition.x, (int)target.localPosition.z, 2, 0);
        parent.SetActive(false);
    }
}
