﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ui_buttonSelectMusicBackground : MonoBehaviour {

    public Button yourButton;
    public AudioSource fxSound;
    public AudioClip backMusic;
    public string textMusic;
    public TextMeshProUGUI text;

    void selectusicSorce()
    {
        GameObject[] allSorce = GameObject.FindGameObjectsWithTag("Music");
        if (allSorce.Length <= 0)
            return;
        int i = 0;
        while (fxSound == null && i < allSorce.Length)
            fxSound = allSorce[i++].GetComponent<AudioSource>();
    }

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        if (fxSound == null)
            selectusicSorce();
        if (fxSound == null)
            return;
        fxSound.Stop();
        fxSound.clip = backMusic;
        fxSound.Play();
        text.text = textMusic;
        //fxSound.Play();
    }
}
