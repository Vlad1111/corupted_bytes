﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DataInventory : MonoBehaviour
{
    public TextMeshProUGUI text;
    void Start()
    {
        transform.gameObject.SetActive(false);
    }

    public void setData(int[] data, string auxString)
    {
        transform.gameObject.SetActive(true);
        for (int i = 0; i < data.Length; i++)
            if (data[i] != 0)
            {
                auxString += "Data typr " + i + "  :    " + data[i] + "\n";
            }
        text.text = auxString;
    }
}
