﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class objectMeniuObtions : MonoBehaviour
{
    public Transform target;
    public createWord takeCamera;
    private Camera mainCamera;
    private new RectTransform transform;

    void getSetCamera()
    {
        mainCamera = takeCamera.getThePlayerCamera();
    }

    void Start()
    {
        getSetCamera();
        transform = GetComponent<RectTransform>();
    }

    public void getTarget(Transform newTarget)
    {
        transform.gameObject.SetActive(true);
        target = newTarget;
    }

    void FixedUpdate()
    {
        if (mainCamera == null)
            getSetCamera();
        if(mainCamera==null || target==null)
        {
            transform.gameObject.SetActive(false);
        }
        else
        {
            Vector2 poz = mainCamera.WorldToScreenPoint(target.position);
            transform.position = poz;
        }
    }
}
