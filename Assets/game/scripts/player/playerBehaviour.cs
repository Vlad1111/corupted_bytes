﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class playerBehaviour : MonoBehaviour
{
    NPC_movement playerMovement;
    void Star()
    {
        playerMovement = GetComponent<NPC_movement>();
    }
    public void muveTo(float x, float y)
    {
        playerMovement.moveTo(x, y);
    }

    public Camera getPlayerCamera(Transform parent)
    {
        foreach(Transform child in parent)
        {
            Camera camera = child.GetComponentInChildren<Camera>();
            if (camera != null)
                return camera;
            camera = getPlayerCamera(child);
            if (camera != null)
                return camera;
        }
        return null;
    }
}
