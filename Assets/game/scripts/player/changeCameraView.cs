﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class changeCameraView : MonoBehaviour {
    public createWord getTheMainCameraFFS;
    public Button yourButton;
    public Slider slider;
    private Camera mainCamera;
    private int cameraTipe = 1;
    private Vector3 InitialCameraRotation;
    private Transform cameraTransform;

    private void setCamera()
    {
        mainCamera = getTheMainCameraFFS.getThePlayerCamera();
        if (mainCamera == null)
            return;
        cameraTransform = mainCamera.transform.parent;
        if (cameraTransform == null)
            cameraTransform = mainCamera.transform;
        InitialCameraRotation = cameraTransform.localRotation.eulerAngles;
    }

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        setCamera();
    }

    private void FixedUpdate()
    {
        if (mainCamera == null)
            setCamera();
        if (cameraTransform == null)
            return;
        cameraTransform.localScale = new Vector3(slider.value, slider.value, slider.value);
    }

    private void TaskOnClick()
    {
        if (mainCamera == null)
            setCamera();
        if(cameraTipe < 4)
            cameraTransform.localRotation = Quaternion.Euler(InitialCameraRotation + new Vector3(0, 90, 0) * cameraTipe);
        else
            cameraTransform.localRotation = Quaternion.Euler(InitialCameraRotation + new Vector3(90 - InitialCameraRotation.x, 0, 0));
        cameraTipe = (cameraTipe + 1) % 5;
    }
}
