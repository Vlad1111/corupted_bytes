﻿using System;
[System.Serializable]
public class GameObjectBehaviour
{
    private int[] data; // the data that it contain
    private int objectType; // the type:
                            /*
                             * 0 - podea
                             * 1 - perete
                             * 2 - power generator
                             * 3 - kernel
                             * 4 - gate
                             * 5 - trap
                             * 6 - quarantin
                             */
    private int distroyCost; // how much does it take to distroy it
    private int wallValue;

    public GameObjectBehaviour()
    {
        data = new int[1];
        objectType = 0;
        distroyCost = 0;
        wallValue = 0;
    }
    public GameObjectBehaviour(int type, int WallValue)
    {
        data = new int[1];
        objectType = type;
        distroyCost = 0;
        wallValue = WallValue;
    }
    public GameObjectBehaviour(int type, int WallValue, int[] Data, int DistroyCost)
    {
        data = new int[Data.Length];
        for (int i = 0; i < Data.Length; i++)
            data[i] = Data[i];
        objectType = type;
        distroyCost = DistroyCost;
        wallValue = WallValue;
    }

    public int getDistroyCost()
    {
        return distroyCost;
    }

    public void addData(int index, int val)
    {
        if (index < 0 || index >= data.Length)
            return;
        data[index] += val;
    }

    public int getType()
    {
        return objectType;
    }

    public void getData(int[] dataRef, ref int DistryCostRef, ref int key)
    {
        DistryCostRef = distroyCost;
        key = wallValue;
        for (int i = 0; i < dataRef.Length && i < data.Length; i++)
            dataRef[i] = data[i];
    }
    public int getNrData()
    {
        return data.Length;
    }
    public int getWallValue()
    {
        return wallValue;
    }
    public int[] extractData()
    {
        int[] newData = new int[data.Length];
        for (int i = 0; i < data.Length; i++)
        {
            newData[i] = data[i];
            data[i] = 0;
        }
        return newData;
    }

    public void addData(int[] add)
    {
        for (int i = 0; i < add.Length && i < data.Length; i++)
            data[i] += add[i];
    }
}
