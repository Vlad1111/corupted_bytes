﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class clicedOn : MonoBehaviour
{
    void OnMouseOver()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(1))
            {
                objectMeniuObtions sellector = GetComponentInParent<createWord>().obtionMenu;
                sellector.getTarget(transform);
            }
            else if (Input.GetMouseButtonDown(0))
            {
                createWord father = GetComponentInParent<createWord>();
                if (father != null)
                    father.playerClickedOn((int)transform.localPosition.x, (int)transform.localPosition.z, 0, 0);
            }
        }
    }
}