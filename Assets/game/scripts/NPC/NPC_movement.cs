﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_movement : MonoBehaviour {

    private float X, Y;
    private Vector3 directon, rotation;
    private Transform model;

    private void cautaModel()
    {
        model = null;
        foreach (Transform child in transform)
            if (child.name == "model")
                model = child;
        if (model == null)
            model = transform;
    }

    void Start()
    {
        cautaModel();
        X = transform.localPosition.x;
        Y = transform.localPosition.z;
        rotation = model.localRotation.eulerAngles;
    }

    public void moveTo(float x, float y)
    {
        if (X != x || Y != y)
        {
            X = x;
            Y = y;
            Vector3 dis = transform.localPosition - new Vector3(X, transform.localPosition.y, Y);
            directon = dis / generalTerms.generalMumementSpeed;


            rotation = new Vector3(0, Mathf.Atan2(dis.x, dis.z), 0) * 180 / 3.14159265f;
            if (model == null)
                cautaModel();
            if(Mathf.Abs(model.localRotation.eulerAngles.y - rotation.y)>=180)
            {
                if (rotation.y > 0)
                    rotation.y -= 360f;
                else
                    rotation.y += 360f;
            }
            if(rotation.y == -90f && model.localRotation.eulerAngles.y == 0f)
            {
                rotation.y = 270f;
                //model.localRotation = Quaternion.Euler(new Vector3(0, 270, 0));
            }
        }
    }

    void FixedUpdate()
    {
        ///y-ul transformului e de geaba
        if ((X - transform.localPosition.x) * (X - transform.localPosition.x) + 
            (Y - transform.localPosition.z) * (Y - transform.localPosition.z) > 0.0003f)
        {
            transform.localPosition -= directon;
            model.localRotation = Quaternion.Euler(model.localRotation.eulerAngles
                - 3 * (model.localRotation.eulerAngles - rotation) / generalTerms.generalMumementSpeed);
            //transform.Rotate(3.14159265f * rotation / (generalTerms.generalMumementSpeed * 360));
            //Debug.Log(rotation / generalTerms.generalMumementSpeed);
        }
        else if(directon != Vector3.zero)
        {
            transform.localPosition = new Vector3(X, transform.localPosition.y, Y);
            model.localRotation = Quaternion.Euler(rotation);
            directon = Vector3.zero;
        }
    }
}
