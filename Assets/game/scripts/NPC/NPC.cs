﻿[System.Serializable]
public class NPC
{
    private float X = 0; //actual position
    private float Y = 0;
    private int type; //the type of the NPC: 0-player
    private int[] moveToX; //where does it need to go
    private int[] moveToY;
    private int steps; //how many steps does it need to make
    private int restTime = 0; //the speed of it's movement

    private int[] destinationX;
    private int[] destinationY;
    private int destinationIndex;

    private int[] data;
    private int[] recipy;

    private int power;
    private int securityKey;

    public NPC()
    {
        X = 0;
        Y = 0;
        type = 0;
        moveToX = new int[3];
        moveToY = new int[3];
        moveToX[0] = (int)X;
        moveToY[0] = (int)Y;
        steps = 0;
        data = new int[1];
        recipy = new int[1];
        power = 0;
        securityKey = 0;
        destinationX = new int[0];
        destinationY = new int[0];
        destinationIndex = 0;
    }
    public NPC(int tipe, int Power)
    {
        X = 0;
        Y = 0;
        type = tipe;
        moveToX = new int[3];
        moveToY = new int[3];
        moveToX[0] = (int)X;
        moveToY[0] = (int)Y;
        steps = 0;
        data = new int[1];
        recipy = new int[1];
        power = Power;
        securityKey = 0;
        destinationX = new int[0];
        destinationY = new int[0];
        destinationIndex = 0;
    }
    public NPC(int tipe, int Power, int nrData, int[] recipi)
    {
        X = 0;
        Y = 0;
        type = tipe;
        moveToX = new int[3];
        moveToY = new int[3];
        moveToX[0] = (int)X;
        moveToY[0] = (int)Y;
        steps = 0;
        data = new int[nrData + 1];
        recipy = new int[nrData + 1];
        for (int i = 0; i < nrData && i < recipi.Length; i++)
            recipy[i] = recipi[i];
        power = Power;
        securityKey = 0;
        destinationX = new int[0];
        destinationY = new int[0];
        destinationIndex = 0;
    }
    public NPC(int tipe, int Power, int securityKeyVal, int pozX, int pozY, int nrData, int[] recipi)
    {
        X = pozX;
        Y = pozY;
        type = tipe;
        moveToX = new int[3];
        moveToY = new int[3];
        moveToX[0] = (int)X;
        moveToY[0] = (int)Y;
        steps = 0;
        data = new int[nrData + 1];
        recipy = new int[nrData + 1];
        for (int i = 0; i < nrData && i < recipi.Length; i++)
            recipy[i] = recipi[i];
        power = Power;
        securityKey = securityKeyVal;
        destinationX = new int[0];
        destinationY = new int[0];
        destinationIndex = 0;
    }
    public void setDestinations(int[] x, int[] y)
    {
        destinationIndex = x.Length;
        if (destinationIndex > y.Length)
            destinationIndex = y.Length;
        destinationX = new int[destinationIndex];
        destinationY = new int[destinationIndex];
        for(int i=0;i<destinationIndex;i++)
        {
            destinationX[i] = x[i];
            destinationY[i] = y[i];
        }
        destinationIndex = 0;
    }
    public void setRecipy(int[] newRecipy)
    {
        recipy = new int[newRecipy.Length];
        for (int i = 0; i < newRecipy.Length; i++)
            recipy[i] = newRecipy[i];
    }
    public int[] getData()
    {
        return data;
    }
    public int getDestinationIndex()
    {
        return destinationIndex;
    }
    public int[] getDestinationsX()
    {
        return destinationX;
    }
    public int[] getDestinationsY()
    {
        return destinationY;
    }

    public void setDestinationIndex(int val)
    {
        int min = destinationX.Length;
        if (min > destinationY.Length)
            min = destinationY.Length;
        if(min==0)
        {
            destinationIndex = 0;
            return;
        }
        destinationIndex = val%min;        
    }

    public void getPosition(ref float x, ref float y)
    {
        x = X;
        y = Y;
    }
    public int getType()
    {
        return type;
    }
    public int nrSteps()
    {
        return steps;
    }
    public int getRemaingingPower()
    {
        return power;
    }
    public int getSecurityKey()
    {
        return securityKey;
    }


    public void popMovement(ref int x, ref int y)
    {
        x = moveToX[0];
        y = moveToY[0];
        if (steps > 0)
        {
            for (int i = 0; i < steps; i++)
            {
                moveToX[i] = moveToX[i + 1];
                moveToY[i] = moveToY[i + 1];
            }
            steps--;
        }
    }

    public int nextStep()
    {
        if (restTime != 0)
        {
            restTime--;
            return 1;
        }
        if (steps == 0)
            return 2;
        int x, y;
        x = y = 0;
        popMovement(ref x, ref y);
        //throw new System.ArgumentException(x + " " + y + "");
        X = x;
        Y = y;
        restTime = (int)generalTerms.generalMumementSpeed;
        return 0;
    }

    public int getSteps()
    {
        return steps;
    }
    public int[] getmovementX()
    {
        return moveToX;
    }
    public int[] getmovementY()
    {
        return moveToY;
    }
    public int[] getDestinationX()
    {
        return destinationX;
    }
    public int[] getDestinationY()
    {
        return destinationY;
    }

    public void pushMovement(int x, int y)
    {
        int[] mvx, mvy;
        mvx = new int[steps + 5];
        mvy = new int[steps + 5];
        for(int i=0;i<steps;i++)
        {
            mvx[i] = moveToX[i];
            mvy[i] = moveToY[i];
        }
        mvx[steps] = x;
        mvy[steps] = y;
        steps++;
        moveToX = new int[steps + 4];
        moveToY = new int[steps + 4];
        for (int i = 0; i < steps; i++)
        {
            moveToX[i] = mvx[i];
            moveToY[i] = mvy[i];
            //if(i==steps-1)
            //throw new System.ArgumentException(steps + "");
        }
    }

    public void restartPosition(bool moveFromeHere)
    {
        if (steps > 0 && !moveFromeHere)
        {
            X = moveToX[steps - 1];
            Y = moveToY[steps - 1];
        }
        
        moveToX = new int[3];
        moveToY = new int[3];
        moveToX[0] = (int)X;
        moveToY[0] = (int)Y;
        steps = 0;
    }

    public int changeData(int dataCreateIndex, int nrChange)
    {
        int ok = 0;
        for (int i = 0; i < data.Length; i++)
            if (data[i] < recipy[i])
                ok = 1;
        if(ok==0)
        {
            for (int i = 0; i < data.Length; i++)
                data[i] -= recipy[i];
            data[dataCreateIndex] += nrChange;
            return 0;
        }
        return 1;
    }
    public void addData(int[] addData)
    {
        //string str = "";
        for (int i = 0; i < addData.Length && i < data.Length; i++)
        {
            data[i] += addData[i];
            //str += addData[i] + "  ";
        }
        //throw new System.Exception(str);
    }

    public void changePower(int addVal)
    {
        power += addVal;
    }

    public void changeSecurityKey(int newVal)
    {
        securityKey = newVal;
    }

    public int[] getRecipy()
    {
        return recipy;
    }
}
