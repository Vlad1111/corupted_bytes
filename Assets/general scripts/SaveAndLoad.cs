﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoad : MonoBehaviour
{
    public World worldToSave = new World();
    public string[] savedFile;
    public string saveTo = "";

    public void setSaveTo()
    {
        saveTo = Application.dataPath + "/SavedFile";
    }

    public void Awake()
    {
        setSaveTo();

        if (!Directory.Exists(saveTo))
            return;
        if (!File.Exists(saveTo + "/SavedFilesData/Names"))
            return;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(saveTo + "/SavedFilesData/Number", FileMode.Open);
        int nrLev=(int)bf.Deserialize(file);
        file.Close();
        savedFile = new string[nrLev];

        file = File.Open(saveTo + "/SavedFilesData/Names", FileMode.Open);
        savedFile = (string[])bf.Deserialize(file);
        file.Close();

    }

    public void copyWorld(int height, int weid, int[,] map, NPC[] NPCS)
    {
        worldToSave = new World();
        worldToSave.size = new WorldSizes();
        worldToSave.size.HEIGHT = height;
        worldToSave.size.WHEID = weid;
        worldToSave.map = new int[worldToSave.size.HEIGHT, worldToSave.size.WHEID];
        for (int i = 0; i < worldToSave.size.HEIGHT; i++)
            for (int j = 0; j < worldToSave.size.WHEID; j++)
                worldToSave.map[i, j] = map[i, j];

        worldToSave.size.nrNPC = NPCS.Length;
        worldToSave.NPCS = new NPC[worldToSave.size.nrNPC];
        for (int i = 0; i < worldToSave.size.nrNPC; i++)
            worldToSave.NPCS[i] = NPCS[i];
    }

    public void Save(string nameFile)
    {
        //Debug.Log(saveTo);
        if (!Directory.Exists(saveTo))
            Directory.CreateDirectory(saveTo);
        if (!Directory.Exists(saveTo + "/" + nameFile))
            Directory.CreateDirectory(saveTo + "/" + nameFile);
        
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(saveTo + "/" + nameFile+"/"+nameFile+".size");
        bf.Serialize(file, worldToSave.size);
        file.Close();

        file = File.Create(saveTo + "/" + nameFile + "/" + nameFile + ".map");
        bf.Serialize(file, worldToSave);
        file.Close();

        addFileName(nameFile);
    }

    public void Load(string nameFile)
    {
        if (!Directory.Exists(saveTo))
            return;
        if (!File.Exists(saveTo + "/" + nameFile + "/" + nameFile + ".size"))
            return;
        if (!File.Exists(saveTo + "/" + nameFile + "/" + nameFile + ".map"))
            return;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(saveTo + "/" + nameFile + "/" + nameFile + ".size", FileMode.Open);
        worldToSave.size = (WorldSizes)bf.Deserialize(file);
        worldToSave.map = new int[worldToSave.size.HEIGHT, worldToSave.size.WHEID];
        worldToSave.NPCS = new NPC[worldToSave.size.nrNPC];
        file.Close();


        file = File.Open(saveTo + "/" + nameFile + "/" + nameFile + ".map", FileMode.Open);
        worldToSave = (World)bf.Deserialize(file);
        //worldToSave.map = new int[worldToSave.size.HEIGHT, worldToSave.size.WHEID];
        file.Close();
    }

    void addFileName(string nameFile)
    {
        for (int i = 0; i < savedFile.Length; i++)
            if (savedFile[i] == nameFile)
                return;

        string[] ausSaveFile = new string[savedFile.Length];
        for (int i = 0; i < savedFile.Length; i++)
            ausSaveFile[i] = savedFile[i];

        savedFile = new string[savedFile.Length + 1];
        for (int i = 0; i < ausSaveFile.Length; i++)
            savedFile[i] = ausSaveFile[i];
        savedFile[ausSaveFile.Length] = nameFile;



        //Debug.Log("PULA");
        if (!Directory.Exists(saveTo + "/SavedFilesData"))
            Directory.CreateDirectory(saveTo + "/SavedFilesData");

        //Debug.Log("PULA");
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(saveTo + "/SavedFilesData/Names");
        bf.Serialize(file, savedFile);
        file.Close();

        //Debug.Log("PULA");
        file = File.Create(saveTo + "/SavedFilesData/Number");
        bf.Serialize(file, savedFile.Length);
        file.Close();
    }
}

[System.Serializable]
public class WorldSizes
{
    public int HEIGHT=0, WHEID = 0;
    public int nrNPC = 0;
}

[System.Serializable]
public class World
{
    public WorldSizes size;
    public int[,] map;
    public NPC[] NPCS;
}