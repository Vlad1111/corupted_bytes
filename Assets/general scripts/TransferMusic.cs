﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransferMusic : MonoBehaviour
{
    private static TransferMusic instance;
    private static TransferMusic Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}