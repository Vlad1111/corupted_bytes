﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createMovingLIneNPC : MonoBehaviour
{
    public void addLine(Vector2 poz)
    {
        LineRenderer line = GetComponent<LineRenderer>();
        if(line == null)
        {
            line = gameObject.AddComponent(typeof(LineRenderer)) as LineRenderer;
            line.SetPositions(new Vector3[0]);
        }
        if (line.positionCount!=0 && line.GetPosition(line.positionCount-1) == new Vector3(poz.x, poz.y, -0.1f))
            return;
        line.positionCount++;
        line.SetPosition(line.positionCount - 1, new Vector3(poz.x, poz.y, -0.1f));
    }

    public int[] getX()
    {
        LineRenderer line = GetComponent<LineRenderer>();
        int[] x = new int[line.positionCount];
        for (int i = 0; i < line.positionCount; i++)
            x[i] = (int)line.GetPosition(i).x;
        return x;
    }

    public int[] getY()
    {
        LineRenderer line = GetComponent<LineRenderer>();
        int[] x = new int[line.positionCount];
        for (int i = 0; i < line.positionCount; i++)
            x[i] = (int)line.GetPosition(i).y;
        return x;
    }
}
