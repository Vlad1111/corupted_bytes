﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonLoadMapEditor : MonoBehaviour
{
    public Dropdown dropdown;
    public InputField inputField;
    public Button yourButton;
    public editWorld editWorld;
    public SaveAndLoad SaL;

    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        dropdown.ClearOptions();
        List<Dropdown.OptionData> obt = new List<Dropdown.OptionData>();
        for (int i = 0; i < SaL.savedFile.Length; i++)
            obt.Add(new Dropdown.OptionData(SaL.savedFile[i]));
        dropdown.AddOptions(obt);
    }

    private void TaskOnClick()
    {
        SaL.Load(dropdown.options[dropdown.value].text);
        editWorld.HEIGHT = SaL.worldToSave.size.HEIGHT;
        editWorld.WHIDE = SaL.worldToSave.size.WHEID;

        editWorld.map = new GameObjectBehaviour[editWorld.HEIGHT, SaL.worldToSave.size.WHEID];
        editWorld.buildObtion.ClearOptions();
        editWorld.buildObtion.options.Add(new Dropdown.OptionData("Delete NPC"));
        editWorld.NPCS.Clear();
        for (int i = 0; i < SaL.worldToSave.size.nrNPC; i++)
        {
            float x, y;
            x = y = 0;
            SaL.worldToSave.NPCS[i].getPosition(ref x, ref y);
            editWorld.NPCS.Add(new NPC(SaL.worldToSave.NPCS[i].getType(), 100, 10, (int)x, (int)y, 10, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            editWorld.NPCS[i].setDestinations(SaL.worldToSave.NPCS[i].getDestinationsX(), SaL.worldToSave.NPCS[i].getDestinationsY());


            editWorld.buildObtion.options.Add(new Dropdown.OptionData("NPC" + (i + 1)));
        }
        editWorld.reloadMap(SaL.worldToSave.map);

        inputField.text = dropdown.options[dropdown.value].text;
    }
}
