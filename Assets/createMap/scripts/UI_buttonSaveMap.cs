﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_buttonSaveMap : MonoBehaviour {

    //public Dropdown dropdown;
    public InputField inputField;
    public Button yourButton;
    public editWorld editWorld;
    public SaveAndLoad SaL;

    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()
    {
        if (inputField.text == "")
            return;
        /*SaL.worldToSave.size.HEIGHT = editWorld.HEIGHT;
        SaL.worldToSave.size.WHEID = editWorld.WHIDE;
        SaL.worldToSave.map = new int[editWorld.HEIGHT, editWorld.WHIDE];
        for (int i=0;i< editWorld.HEIGHT; i++)
            for(int j=0; j< editWorld.WHIDE;j++)
            {
                SaL.worldToSave.map[i, j] = editWorld.map[i, j].getType();
            }
        SaL.worldToSave.size.nrNPC = 0;
        SaL.worldToSave.NPCS = new NPC[1];*/
        int[,] map = new int[editWorld.HEIGHT, editWorld.WHIDE];
        for (int i = 0; i < editWorld.HEIGHT; i++)
            for (int j = 0; j < editWorld.WHIDE; j++)
                map[i, j] = editWorld.map[i, j].getType();
        NPC[] NPCS = new NPC[editWorld.NPCS.Count];
        for (int i = 0; i < editWorld.NPCS.Count; i++)
        {
            float xx, yy;
            xx = yy = 0;
            editWorld.NPCS[i].getPosition(ref xx, ref yy);
            NPCS[i] = new NPC(editWorld.NPCS[i].getType(), editWorld.NPCS[i].getRemaingingPower(), 
                editWorld.NPCS[i].getSecurityKey(), (int)xx, (int)yy, 10, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            NPCS[i].setDestinations(editWorld.NPCS[i].getDestinationsX(), editWorld.NPCS[i].getDestinationsY());
        }
        SaL.copyWorld(editWorld.HEIGHT, editWorld.WHIDE, map, NPCS);

        SaL.Save(inputField.text);
    }
}
