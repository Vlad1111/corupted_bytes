﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonCenterCamereOnNPC : MonoBehaviour {

    public Button yourButton;
    public editWorld edW;
    public Transform myCamera;
    void Start()
    {
        if (yourButton == null)
        {
            yourButton = GetComponent<Button>();
            if (yourButton == null)
                return;
        }
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);


    }
	
    public void TaskOnClick()
    {
        if (edW.buildObtion.value == 0)
            return;
        Vector3 poz = myCamera.localPosition;

        edW.NPCS[edW.buildObtion.value - 1].getPosition(ref poz.x, ref poz.y);

        myCamera.localPosition = poz;
    }
}
