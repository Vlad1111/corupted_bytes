﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_buttonMakeNew : MonoBehaviour
{
    public Button yourButton;
    public editWorld editWorld;
    public InputField HEIGHT;
    public InputField WHEID;
    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()
    {
        editWorld.HEIGHT = generalTerms.stringToInt(HEIGHT.text);
        editWorld.WHIDE = generalTerms.stringToInt(WHEID.text);

        editWorld.map = new GameObjectBehaviour[editWorld.HEIGHT, editWorld.WHIDE];
        for (int i = editWorld.NPCS.Count - 1; i >= 0; i--)
            editWorld.NPCS.Remove(editWorld.NPCS[i]);
        //editWorld.NPCS.Clear();
        editWorld.reloadMap(new int[editWorld.HEIGHT, editWorld.WHIDE]);
    }
}
