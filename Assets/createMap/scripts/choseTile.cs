﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class choseTile : MonoBehaviour {

    public Button yourButton;
    public int tileIndex;
    public editWorld edit;

    private void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    private void TaskOnClick()
    {
        edit.tileChangeIndex = tileIndex;
    }
}
