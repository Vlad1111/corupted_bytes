﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clickedOnEdit : MonoBehaviour {
    private editWorld edit;

    private void Start()
    {
        edit = GetComponentInParent<editWorld>();
    }

    public void wasClickedOn()
    {
        edit.change((int)transform.localPosition.x, (int)transform.localPosition.y);
    }
}
