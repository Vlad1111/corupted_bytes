﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class editWorld : MonoBehaviour
{
    public Transform emptyGameObjectPrefab;
    public Transform[] objs;
    public Transform[] NPCS_mesh;
    public int tileChangeIndex = 0; // the tile that is nedded to be chang into

    public Dropdown buildObtion;

    public GameObjectBehaviour[,] map;
    public int HEIGHT, WHIDE;
    public List<NPC> NPCS;
    public List<createMovingLIneNPC> NPC_movement;


    private pipeServer_Unity cmds;
    void Start()
    {
        int[,] MAP = new int[generalTerms.map_HEIGHT, generalTerms.map_WHIDE];
        map = new GameObjectBehaviour[generalTerms.map_HEIGHT, generalTerms.map_WHIDE];
        cmds = GetComponent<pipeServer_Unity>();
        if (cmds == null)
        {
            cmds = gameObject.AddComponent(typeof(pipeServer_Unity)) as pipeServer_Unity;
        }
        int Hei, Whid;
        Hei = Whid = 0;
        cmds.getMap(MAP, ref Hei, ref Whid);
        HEIGHT = Hei;
        WHIDE = Whid;

        reloadMap(MAP);
    }

    public void change(int x, int y)
    {
        if (tileChangeIndex >= 0)
        {
            Transform tile = transform.Find(x + "_" + y);
            if (tile != null)
                if (map[x, y].getType() != tileChangeIndex)
                {
                    Destroy(tile.gameObject);
                    Instantiate(objs[tileChangeIndex], new Vector3(x, y, 0),
                        objs[tileChangeIndex].transform.localRotation, transform).name = x + "_" + y;
                    if (tileChangeIndex == 0)
                        map[x, y] = new GameObjectBehaviour(tileChangeIndex, 0);
                    else if (tileChangeIndex != 4)
                        map[x, y] = new GameObjectBehaviour(tileChangeIndex, 10000);
                    else if (tileChangeIndex == 4)
                        map[x, y] = new GameObjectBehaviour(tileChangeIndex, 10);
                }
        }
        else
        {
            if (buildObtion.value == 0)
            {
                // Debug.Log(-tileChangeIndex + 1);
                Transform tile = transform.Find("N_" + x + "_" + y);
                if (tile != null)
                {
                    Destroy(tile.gameObject);
                    for (int i = 0; i < NPCS.Count; i++)
                    {
                        float xxx, yyy;
                        xxx = yyy = 0;
                        NPCS[i].getPosition(ref xxx, ref yyy);
                        if ((int)xxx == x && (int)yyy == y)
                        {
                            NPCS.Remove(NPCS[i]);
                            Destroy(NPC_movement[i].transform.gameObject);
                            NPC_movement.Remove(NPC_movement[i]);
                            break;
                        }
                    }
                    buildObtion.options.Remove(buildObtion.options[buildObtion.options.Count - 1]);
                }
                if (-tileChangeIndex - 1 < NPCS_mesh.Length)
                {
                    /*Instantiate(NPCS_mesh[-tileChangeIndex - 1], new Vector3(x, y, 0),
                          NPCS_mesh[-tileChangeIndex - 1].transform.localRotation, transform).name = "N_" + x + "_" + y;
    */
                    Transform npc = Instantiate(NPCS_mesh[-tileChangeIndex - 1], new Vector3(x, y - 0.5f, 0),
                            NPCS_mesh[-tileChangeIndex - 1].transform.localRotation, transform);
                    npc.name = "N_" + x + "_" + y;
                    npc.localScale = new Vector3(npc.localScale.x * 0.5f, npc.localScale.y * 0.5f, npc.localScale.z * 0.5f);
                    NPCS.Add(new NPC(-tileChangeIndex, 100, 10, x, y, 10, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
                    buildObtion.options.Add(new Dropdown.OptionData("NPC" + NPCS.Count));

                    //GameObject line = gameObject.AddComponent<GameObject>();
                    GameObject line = Instantiate(emptyGameObjectPrefab, new Vector3(x, y - 0.5f, 0), Quaternion.Euler(0, 0, 0), transform).gameObject;
                    line.transform.localPosition = new Vector3(x, y - 0.5f, 0);
                    line.AddComponent<LineRenderer>().positionCount = 0;
                    line.GetComponent<LineRenderer>().widthMultiplier = 0.1f;
                    line.tag = "NPC";
                    NPC_movement.Add(line.AddComponent<createMovingLIneNPC>());
                }
            }
            else
            {
                int nr = buildObtion.value - 1;
                NPC_movement[nr].addLine(new Vector2(x, y));
                NPCS[nr].setDestinations(NPC_movement[nr].getX(), NPC_movement[nr].getY());
            }
        }
    }

    public void reloadMap(int[,] MAP)
    {
        GameObject[] old = GameObject.FindGameObjectsWithTag("Tile");
        for (int i = 0; i < old.Length; i++)
            Destroy(old[i]);
        old = GameObject.FindGameObjectsWithTag("NPC");
        NPC_movement.Clear();
        for (int i = 0; i < old.Length; i++)
            Destroy(old[i]);

        for (int i = 0; i < HEIGHT; i++)
            for (int j = 0; j < WHIDE; j++)
            {
                //if (MAP[i, j] == 0)
                Instantiate(objs[MAP[i, j]], new Vector3(i, j, 0), objs[MAP[i, j]].transform.localRotation, transform).name = i + "_" + j;
                map[i, j] = new GameObjectBehaviour(MAP[i, j], 0);
            }

        for (int i = 0; i < NPCS.Count; i++)
        {
            float xxx = 0, yyy = 0;
            NPCS[i].getPosition(ref xxx, ref yyy);
            Transform npc = Instantiate(NPCS_mesh[NPCS[i].getType() - 1], new Vector3(xxx, yyy - 0.5f, 0),
                    NPCS_mesh[NPCS[i].getType() - 1].transform.localRotation, transform);
            npc.name = "N_" + (int)xxx + "_" + (int)yyy;
            npc.localScale = new Vector3(npc.localScale.x * 0.5f, npc.localScale.y * 0.5f, npc.localScale.z * 0.5f);

            GameObject line = Instantiate(emptyGameObjectPrefab, new Vector3(xxx, yyy - 0.5f, 0), Quaternion.Euler(0, 0, 0), transform).gameObject;
            line.transform.localPosition = new Vector3(xxx, yyy - 0.5f, 0);
            line.AddComponent<LineRenderer>().positionCount = 0;
            line.GetComponent<LineRenderer>().widthMultiplier = 0.1f;
            line.tag = "NPC";
            NPC_movement.Add(line.AddComponent<createMovingLIneNPC>());

            int[] x, y;
            x = NPCS[i].getDestinationsX();
            y = NPCS[i].getDestinationsY();
            for (int j = 0; j < x.Length && j < y.Length; j++)
                line.GetComponent<createMovingLIneNPC>().addLine(new Vector2(x[j], y[j]));
        }
    }
}
