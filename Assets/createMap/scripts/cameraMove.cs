﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class cameraMove : MonoBehaviour
{
    private Vector3 mousePosition = Vector3.zero;
    private float wheelValue;

    private void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            if (Input.GetMouseButton(0))
            { // if left button pressed...
                Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    // the object identified by hit.transform was clicked
                    // do whatever you want
                    hit.transform.GetComponent<clickedOnEdit>().wasClickedOn();
                }
            }


        wheelValue = 10 * Input.GetAxis("Mouse ScrollWheel");
        if (wheelValue != 0f && transform.localPosition.z + wheelValue < -2f && transform.localPosition.z + wheelValue >= -49f)
        {
            // Debug.Log((-2f + 3 * wheelValue) + " " + transform.localPosition.z + " " + (-45f + 3 * wheelValue));
            transform.localPosition += new Vector3(0, 0, wheelValue);
            //whee
        }
        if (Input.GetMouseButtonDown(1))
        {
            mousePosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(1))
        {
            Vector3 newMousePoz = Input.mousePosition;
            transform.localPosition += 2 * (mousePosition - newMousePoz) / (51 - transform.localPosition.z);
            mousePosition = newMousePoz;
        }

    }
}
